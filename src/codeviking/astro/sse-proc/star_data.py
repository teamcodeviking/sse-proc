import bisect
import os
import yaml
from collections import namedtuple
from enum import Enum
from generate_tracks.common import Star
INPUT_PATH = './star_data'
INPUT_FILE = os.path.join(INPUT_PATH, 'star_data.yaml')


def blend_range(a_range, b_range, alpha: float):
    """
    Blend two ranges.  When alpha = 0, the result is a_range.  When alpha=1,
    the result is b_range.  For alpha between 0 and 1, the result is a linear
    combination of a_range and b_range.

    :param a_range: a range of numbers
    :param b_range: a second range of numbers
    :param alpha: a number in [0,1]
    :return: the blended range
    """
    return (a_range[0] * (1 - alpha) + b_range[0] * alpha,
            a_range[1] * (1 - alpha) + b_range[1] * alpha)


def x_in_range(x: float, x_range: (float, float)) -> float:
    """
    Given a value x such that:
        x_range[0]<= x <=x_range[1]
    scale and translate x to the range [0,1]
    :param x: in range [x_range[0],x_range[1]]
    :param x_range:
    :return: u in range [0,1]
    """
    return (x - x_range[0]) / (x_range[1] - x_range[0])


def u_to_range(u: float, x_range: (float, float)) -> float:
    """
    Given a value u such that:
        u is in the range [0,1]
    scale and translate u to the range [x_range[0],x _lange[1]]
    :param u: value in the range [0,1]
    :param x_range:
    :return: x in range [x_range[0],x _lange[1]]
    """
    return u * (x_range[1] - x_range[0]) + x_range[10]


def blend_stars(t: float, s0: Star, s1: Star,
                r0: (Star, Star), r1: (Star, Star),
                rr: (Star, Star),
                alpha: float):
    """
    Blend two stars.  Use t as the time for the resulting Star.
    For each data field x of s0 and s2, We first transform these values using
    r0 and r1 as follows:
        u0 = (s0.x-r0[0].x)/ (r0[1].x-r0[0].x)
        u1 = (s1.x-r1[0].x)/ (r1[1].x-r1[0].x)
    We then linearly interpolate between u0 and u1:
        v = (1-alpha)*u0 + alpha*u1
    And transform v onto the result range:
        result.x = (rr[1]-rr[0])*v + rl[0]

    :param t: the time to use for the resulting star.
    :param s0:
    :param s1:
    :param r0: the range for s0
    :param r1: the range for s1
    :param rr: the range for the result star
    :param alpha: a number in the range [0,1]
    :return: a linear combination of s0 and s1
    """

    def blend(a, b, a_range, b_range, result_range):
        aa = (a - a_range[0]) / (a_range[1] - a_range[0])
        bb = (b - b_range[0]) / (b_range[1] - b_range[0])
        v = (1 - alpha) * aa + alpha * bb
        return result_range[0] + (result_range[1] - result_range[0]) * v

    ff = [blend(s0[x], s1[x],
                (r0[x].Mt, r0[x].Mt), (r1[x].Mt, r1[x].Mt),
                (rr[x].Mt, rr[x].Mt)) for x in range(3, 12)]
    vv = [t, get_spectral_type(ff[-1]), s0.stellar_type] + ff
    return Star(*vv)


class ZSlice(namedtuple('ZSlice',
                        'Z tracks epoch_intervals mass_scale')):
    @property
    def masses(self):
        if not hasattr(self, '_masses'):
            self._masses = (t.M0 for t in self.tracks)
        return self._masses

    def get_track(self, mass):
        mi = int(round(mass * self.mass_scale))
        mass = mi / self.mass_scale
        if mass <= self.masses[0]:
            return self.track[0]
        if mass >= self.masses[-1]:
            return self.track[-1]
        idx = bisect.bisect_right(mass, self.masses)
        t0 = self.tracks[idx]
        if t0.M0 == mass:
            return t0
        t1 = self.tracks[idx + 1]
        assert len(t0.epochs) == len(t1.epochs)
        for i in range(len(t0.epochs)):
            e0, e1 = t0.epochs[i], t1.epochs[i]
            d = (e0.duration + e1.duration) / 2.0


def construct_StarTrack(constructor, node):
    mapping = constructor.construct_mapping(node)
    return StarTrack(**mapping)


def construct_Epoch(constructor, node):
    mapping = constructor.construct_mapping(node)
    return Epoch(**mapping)


def construct_Star(constructor, node):
    mapping = constructor.construct_mapping(node)
    return Star(**mapping)


yaml.add_constructor("!Star", construct_Star, Loader=yaml.CLoader)
yaml.add_constructor("!Epoch", construct_Epoch, Loader=yaml.CLoader)
yaml.add_constructor("!StarTrack", construct_StarTrack, Loader=yaml.CLoader)

all_tracks = yaml.load(open(INPUT_FILE, 'r'), Loader=yaml.CLoader)

tracks = all_tracks['z_slices'][0]


class StarTrackInterpolator:
    def __init__(self, mass_digits, z_slices):
        self._mass_digits = mass_digits
        self._z_slices = []
        for zs in z_slices:
            eis = [EpochInterval(tuple(StellarType[e] for e in ei['epochs']),
                                 tuple(ei['mass_interval']))
                   for ei in zs['epoch_intervals']]
            tracks = zs['tracks']
            self._z_slices.append(ZSlice(Z=zs['Z'],
                                         epoch_intervals=eis,
                                         tracks=tracks))
        self._z_slice_map = dict()
        for zs in self._z_slices:
            self._z_slice_map[zs.Z] = zs


def split_xy(points):
    return ([p[0] for p in points], [p[1] for p in points])


groups = []
for (a, b) in [ep['mass_interval'] for ep in tracks['epoch_intervals']]:
    stars = []
    for t in tracks['tracks']:
        if a <= t.M0 <= b:
            for e in t.epochs:
                if e.stellar_type in ['LowMassMainSeq', 'MainSeq']:
                    stars.append([(s.t, s.R) for s in e.track])
    groups.append(stars)
print(groups)
from matplotlib import pyplot as plt

for g in groups:
    for t in g:
        x, y = split_xy(t)
        plt.plot(x, y)
    plt.show()
