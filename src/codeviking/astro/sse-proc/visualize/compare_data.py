import bz2, multiprocessing, os, pprint, sys, traceback
from collections import defaultdict
from copy import deepcopy
from numpy import array, sqrt, amax, log10, zeros

P = pprint.PrettyPrinter(indent=4)
stellar_types = {0: 'LowMassMainSeq' ,
        1: 'MainSeq',
        2: 'HertzsprungGap' ,
        3: 'GiantBranch' ,
        4: 'CoreHe' ,
        5: 'AGB1' ,
        6: 'AGB2' ,
        7: 'NakedHeMS',
        8: 'NakedHeHG',
        9: 'NakedHeGB',
        10: 'WhiteDwarfHe' ,
        11: 'WhiteDwarfCO' ,
        12: 'WhiteDwarfON' ,
        13: 'NeutronStar' ,
        14: 'BlackHole' ,
        15: 'MasslessSupernova'}

element_labels = ['t', 'M', 'Mt', 'L_log10', 'R_log10', 'Teff_log10', 'Mcore', 'Menv', 'Rcore_log10', 'Renv_log10', 'epoch', 'spin']
TOL = 1E-3

def array_to_str(A):
    A = [list(r) for r in A]
    return '\n'.join(['\t'.join(["%+0.8e" % f for f in line])
                     for line in A]
                    )

# There's a better way to define the error.  This just computes the sum of the
# relative errors for each column of data.  This means it's sensitive to the
# length of the data, and doesn't find outliers very well, but it's probably
# good enough
MAX_ERROR = 1E-9

def compare(data1, data2):
#    print "comparing data for M=%f, Z=%f" % (data1['mass'], data1['metallicity']),

#    print '*' * 80
#    print data1['mass'], data1['metallicity']

    d1 = data1['data']
    d2 = data2['data']
#    print list(d1[0:3])
#    print list(d2[0:3])
    n = min(len(d1), len(d2))
    d1 = array(d1[:n])
    d2 = array(d2[:n])
#    print '=' * 80
#    print array_to_str(d1[0:5])
#    print '-' * 80
#    print array_to_str(d2[0:5])
#    print '-' * 80
#    print array_to_str((d1 - d2)[0:5])
#    print '=' * 80

    d1 = d1.transpose()
    d2 = d2.transpose()
    assert d1.shape == d2.shape
    nc = d1.shape[0]
    e_avg = []
    e_min = []
    e_max = []
    exceeded = []
    for c in range(nc):
        delta = abs(d1[c] - d2[c])
        B = array([d1[c], d2[c]])
        mx = list(amax(B, 0))
        mx = array([v if v > 0 else 1 for v in mx])
        delta /= mx
        delta = abs(delta)
        if (delta > MAX_ERROR).any():
            exceeded.append(c)
        e_avg.append(sum(delta) / len(delta))
        e_min.append(min(delta))
        e_max.append(max(delta))

    if exceeded:
        return (data1['mass'], data1['metallicity'], exceeded, e_avg, e_min, e_max)
    else:
        return None


def read_and_compare(fn):
    try:
        t1 = bz2.BZ2File(SRC1 + '/' + fn, 'r').read()
        t2 = bz2.BZ2File(SRC2 + '/' + fn, 'r').read()
        d1 = eval(t1)
        d2 = eval(t2)
        return fn, compare(d1, d2)
    except Exception:
        return fn, traceback.format_exc()
    return fn, None


if __name__ == '__main__':
    SRC1 = 'output'
    SRC2 = 'output.fortran'
    pool = multiprocessing.Pool(processes=8)
    pool_args = []

    sf1 = list(os.listdir(SRC1))
    sf2 = list(os.listdir(SRC2))
    sf1.sort()
    sf2.sort()
    for i in range(len(sf1)):
        assert sf1[i] == sf2[i]
        pool_args.append(sf1[i])

    N = len(pool_args)
    result = pool.imap_unordered(read_and_compare, pool_args)
    success = 0
    runs = 0
    fails = 0
    msgs = []
    sys.stdout.write("\n")
    for fn, item in result:
        runs += 1
        if item:
            msgs.append(item)
            fails += 1
        else:
            success += 1
        sys.stdout.write("\rComparisons: %d/%d - Success: %d  Failures: %d" % (runs, N, success, fails))
        sys.stdout.flush()
    pool.close()
    for item in msgs:
        if isinstance(item, basestring):
            print '\n' + ('=' * 80)
            print "Error while processing: %s" % fn
            print '\n' + ('-' * 80)
            print item
            print '\n' + ('=' * 80)
        else:
            (mass, Z, exceeded, e_avg, e_min, e_max) = item
            print '\n' + ('-' * 80)
            print "comparing data for M=%f, Z=%f" % (mass, Z)
            print "   error threshold exceeded for column(s): %s." % (', '.join(map(str, exceeded)))
            print "   avg error = ", e_avg
            print "   min error = ", e_min
            print "   max error = ", e_max
    pool.join()
