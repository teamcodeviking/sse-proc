import bz2, os, pprint
from collections import defaultdict
from copy import deepcopy
from numpy import array
from curveanalysis import split_and_cull

P = pprint.PrettyPrinter(indent=4)
stellar_types = {0: 'LowMassMainSeq' ,
        1: 'MainSeq',
        2: 'HertzsprungGap' ,
        3: 'GiantBranch' ,
        4: 'CoreHe' ,
        5: 'AGB1' ,
        6: 'AGB2' ,
        7: 'NakedHeMS',
        8: 'NakedHeHG',
        9: 'NakedHeGB',
        10: 'WhiteDwarfHe' ,
        11: 'WhiteDwarfCO' ,
        12: 'WhiteDwarfON' ,
        13: 'NeutronStar' ,
        14: 'BlackHole' ,
        15: 'MasslessSupernova'}
element_labels = ['t', 'M', 'Mt', 'L_log10', 'R_log10', 'Teff_log10', 'Mcore', 'Menv', 'Rcore_log10', 'Renv_log10', 'epoch', 'spin']
TOL = 1E-3

def get_range(data):
    return abs(max(data) - min(data))

def get_ranges(data):
    n = len(data[0])
    R = []
    for i in range(1, n):
        R.append(get_range([elem[i] for elem in data]))
    return R

def cull_track(stype, track):
    track = [elem for elem in track if elem[0] >= 0]
    # return track
    R = TOL * abs(array(get_ranges(track)))
    result = [track[0]]
    last_t = track[0][0]
    real_last_t = track[0][0]
    last = array(track[0][1:])
    real_last = array(track[0][1:])
    for i, elem in enumerate(track[1:-1]):
        a = array(elem[1:])
        a_t = elem[0]
        nxt = array(track[i + 2][1:])
        nxt_t = track[i + 2][0]
        nv = (nxt - a)

        # using the next derivative as an approximation, calculate this point.
        p = (a_t - last_t) * (nv / (nxt_t - a_t))
        # figure out the error
        d = abs(a - p)

#       print 'a=',a
#        print 'last=',last
#       print 'delta=',delta
    #    print 'R=',R
#        print 'big enough=',(abs((a-last))>R),(abs((a-last))>R).all()

        if (d >= R).all():
            result.append(list(elem))
            last = a
            last_t = a_t
    result.append(track[-1])
    return result



def split_data(data, cull=False):
    culls = 0
    elems = 0
    result = {'mass':data['mass'],
              'Z': data['Z']}
    sequence = list()
    tracks = defaultdict(list)
    data = data['data']
    last_type = -1
    ELEM_LEN = None
    for elem in data:
        kt = elem[1]
        elem = elem[0:1] + elem[2:]
        elem = [float(e) for e in elem]
        if ELEM_LEN is None:
            ELEM_LEN = len(elem)
        assert ELEM_LEN == len(elem)
        if kt != last_type:
            # need to make sure that the tracks are connected.  There is often
            # a gap (or even an overlap) between tracks.  We'll adopt the
            # following strategy:  If the end point of the preceding track is
            # not the same as the start point of this track, we either change
            # the endpoint of the previous track to be the same as the start
            # point of this track, OR we add a new point to the previous track
            # (the first point of this track).
            if tracks:
                last_point0 = tracks[stellar_types[last_type]][-1]
                if last_point0 != elem:
                    delta_t = elem[0] - last_point0[0]
                    assert delta_t > 0.0
                    tracks[stellar_types[last_type]].append(deepcopy(elem))
            sequence.append(stellar_types[kt])
            last_type = kt
        if elem[0] >= 0.0:
            tracks[stellar_types[kt]].append(elem)
    result['sequence'] = sequence
    result['tracks'] = dict()



    for stype, track in tracks.items():
        if cull:
            culled_track = dict()
            T = array([e[0] for e in track])
            elements = []
            for i in range(1, ELEM_LEN):
                F = array([e[i] for e in track])
                elems += len(F)
                dummy, subfunctions, region = split_and_cull(T, F)
                functions = []
                for (a, b, knots, spline) in subfunctions:
                    culls += len(F) - len(knots)
                    functions.append({'a':a,
                                     'b':b,
                                     'knots': knots,
                                     'spline':spline.as_dict()})
                elements.append({'region':region, 'functions':functions})
                culled_track[element_labels[i]] = elements
            result['tracks'][stype] = culled_track
        else:
            result['tracks'][stype] = track

    print "culled %d/%d elements" % (culls, elems)

    return result


CULL = False
if CULL:
    OUTPUT_DIR = 'culled'
else:
    OUTPUT_DIR = 'unculled'
output = list(os.listdir('output'))
output.sort()
for f in output:
    print f
    text = bz2.BZ2File('output/' + f, 'r').read()
    text = text.replace('-Infinity', "'-inf'").replace('Infinity', "'inf'")
    data = split_data(eval(text), CULL)
    bz2.BZ2File(OUTPUT_DIR + '/' + f[:-4] + '.py.bz2', 'w').write(P.pformat(data))

