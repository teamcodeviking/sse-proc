import bz2, math, sys
import matplotlib.pyplot as plt
import numpy as np
from curveanalysis import *

stellar_types = {'LowMassMainSeq': '-',
        'MainSeq': '-',
        'HertzsprungGap': '--' ,
        'GiantBranch': '-.' ,
        'CoreHe': '-.' ,
        'AGB1': '-.' ,
        'AGB2': '-.' ,
        'NakedHeMS': '-.',
        'NakedHeHG': '-.',
        'NakedHeGB': '-.',
         'WhiteDwarfHe': ':' ,
         'WhiteDwarfCO': ':' ,
         'WhiteDwarfON': ':' ,
         'NeutronStar': ':' ,
         'BlackHole': ':',
         'MasslessSupernova': ':'}

f1 = sys.argv[1]
c1 = int(sys.argv[2])
c2 = int(sys.argv[3])
d1 = eval(bz2.BZ2File(f1, 'r').read())
track_number = None
if len(sys.argv) > 4:
    track_number = int(sys.argv[4])





def split_tracks(data, x1, x2):
    pairs = []

    for (k, elems) in data['tracks'].items():
        t = np.array([elem[x1] for elem in elems])
        f = [elem[x2] for elem in elems]
        pairs.append((t, f))
    return pairs





NP = 1

tracks = split_tracks(d1, c1, c2)
max_t = 0
for (T, F) in tracks:
    max_t = max(max_t, max(T))
    print "track length=", len(T)


# tracks = [(T / max_t, F) for (T, F) in tracks]
tracks = [(T , F) for (T, F) in tracks]
N = 1
NP = 1
k = 1
ntracks = 0
if track_number is not None:
    tracks = tracks[track_number:track_number + 1]
for T, F in tracks:
    k = 1
    plt.subplot(NP, N, 1)
    track_t = [T[0], T[-1]]
    track_f = [F[0], F[-1]]
    df = calc_derivative(T, F)
    ddf = np.array(calc_derivative2(T, F))
    plt.plot(track_t, track_f, color='r', marker='x', linestyle=' ', markersize=8, markeredgewidth=2)
    discontinuities, subfunctions, region = split_and_cull(T, F)
    df = calc_derivative(T, F)
    ddf = np.array(calc_derivative2(T, F))
    disc_t = [d[0] for d in discontinuities]
    disc_f = [d[1] for d in discontinuities]
    plt.subplot(NP, N, 1)
    plt.plot(T, F, 'k')
    plt.plot(disc_t, disc_f, color='r', marker='x', linestyle=' ', markersize=16, markeredgewidth=3)
    for (a, b, knots, spline) in subfunctions:
        print 'subfunction on [%f,%f]' % (a, b)
        print 'knots=', knots
        plt.plot(*knots[:2], color='g', marker='x', linestyle=' ', markersize=8, markeredgewidth=2)
        ab = np.arange(a, b, (b - a) / 400.0)
        v = [spline(x) for x in ab]
        plt.plot(ab, v, color='orange')
        print "number of knots=", len(knots)
        print "spline=", PP.pformat(spline.as_tuple())


# ,    for (a, b, knots, spline) in subfunctions:
#        print 'subfunction on [%f,%f]' % (a, b)
#        print 'knots=', knots
#        plt.plot(*knots[:2], color='g', marker='x', linestyle=' ', markersize=8, markeredgewidth=2)
#
#        ab = np.arange(a, b, 0.01)
#        v = [spline(x) for x in ab]
#        plt.plot(ab, v, color='orange')

    ntracks += 1
    print "num tracks:", ntracks
plt.show()
