End Stage of Stars by ZAMS Mass
===============================


| Min Mass    | Max Mass      | End Stage        |
|-------------:|-------------:|:-----------------|
|  0.08000      |   0.71849   | White Dwarf (He) |
|  0.71850      |   0.86442   | White Dwarf (CO) |
|  0.86443      |   6.30720   | White Dwarf (ON) |
|  6.30721      |  13.27634   | Neutron Star     |
| 13.27635      | 100.00000   | Black Hole       |
