import math, os, sys, time
import numpy as np
from collections import namedtuple

import yaml

"""
Find the closest datum to the midpoint of a filtered star type data track (
created by filter_star_type_data.py).  Print out the data points for log10_L,
R, and Teff

Takes one argument, the name of the star type data track (first argument to
filter_star_type_data.py).
"""

data_file = 'star_type_data/star_data-%s.yaml' % sys.argv[1]
mass = float(sys.argv[2])
Star = namedtuple('Star',
                  't spectral_type stellar_type  Mt M0 Mc Menv R Rc Renv '
                  'log10_L Teff')
print("reading data file %s ..." % data_file)
tstart = time.time()
star_data = yaml.load(open(data_file, 'r').read(), Loader=yaml.CLoader)
tend = time.time()
print("    read data in %0.2f seconds." % (tend - tstart))


def format_datum(d):
    d = list(d)
    d = d[:8] + [d[8] if d[8] < 1e10 else float('inf')] + d[9:]
    return "%12.3f\t%15s\t%5.1f\t%5.1f\t%5.1f\t%5.1f\t%10.2f\t%10.2f\t%10.2f" \
           "\t%10.2f\t%10.0f" % tuple(
        d)


def get_track(mass):
    print("mass = %f" % mass)
    track = star_data[mass]['epochs']
    print('track length =', len(track))
    print("%12s\t%15s\t%5s\t%5s\t%5s\t%5s\t%10s\t%10s\t%10s\t%10s\t%10s" % (
        't', 'stellar_type', 'Mt', 'Mo', 'Mc', 'Menv', 'R', 'Rc', 'Renv',
        'log10_L',
        'Teff'))
    for epoch in track:
        if len(epoch) == 1:
            print(format_datum(epoch[0]))
        elif len(epoch) > 1:
            print(format_datum(epoch[0]))
            print(format_datum(epoch[-1]))


get_track(mass)


def middle_datum(track):
    if len(track) == 0:
        return None
    print('finding best midpoint for M = %f' % track[0].M)

    all_ages = [t.Tev for t in track]
    if len(all_ages) > 1:
        min_age = min(all_ages)
        max_age = max(all_ages)
        mid_age = ((1 - fraction) * min_age + fraction * max_age)
        candidates = [(abs(t.Tev - mid_age), t) for t in track]
        candidates.sort()
        # grab the closest datum to the mid point of the age span
        _, mid = candidates[0]

    else:
        mid = candidates[0]
    return mid

# stars = []
# for m in sorted(star_data.keys()):
# track = star_data[m]
# d = middle_datum(track)
# if d is not None:
# stars.append(d)

# log10_L = [(d.M, d.log10_L, d.stellar_type) for d in stars]
# R = [(d.M, d.R, d.stellar_type) for d in stars]
# Teff = [(d.M,d.Teff, d.stellar_type) for d in stars]
# tmid = [(d.M, d.Tev, d.stellar_type) for d in stars]
# M0_M = [(d.M0, d.M, d.stellar_type) for d in stars]

# print('log10_L =', log10_L)
# print('R =', R)
# print('Teff =', Teff)
# print('tmid =', tmid)
# print('M0_M =', M0_M)
