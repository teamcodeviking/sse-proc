
class Region(object):
    def __init__(self, t, f):
        if t is None:
            self.t_min, self.t_max = None, None
            self.f_min, self.f_max = None, None
            return
        self.t_min, self.t_max = min(t), max(t)
        self.f_min, self.f_max = min(f), max(f)
        self.f_range = max(self.f_max - self.f_min, 1.0)
        self.t_range = self.t_max - self.t_min
        assert self.t_range > 0.0

    def __call__(self, t, f):
        if self.t_min is None:
            return (t, f)
        return ((t - self.t_min) / self.t_range, (f - self.f_min) / self.f_range)

    def i(self, t, f):
        if self.t_min is None:
            return (t, f)
        return t * self.t_range + self.t_min, f * self.f_range + self.f_min

    def t(self, t):
        if self.t_min is None:
            return t
        return (t - self.t_min) / self.t_range

    def ti(self, t):
        if self.t_min is None:
            return t
        return t * self.t_range + self.t_min

    def df(self, df):
        if self.t_min is None:
            return df
        return df * self.t_range / self.f_range

    def f(self, f):
        if self.t_min is None:
            return f
        return (f - self.f_min) / self.f_range

    def fi(self, f):
        if self.t_min is None:
            return f
        return f * self.f_range + self.f_min

    def as_tuple(self):
        return (self.t_min, self.t_max, self.f_min, self.f_max)

    def as_dict(self):
        return {'t_min': self.t_min, 't_max': self.t_max,
                'f_min':self.f_min, 'f_max':self.f_max}


