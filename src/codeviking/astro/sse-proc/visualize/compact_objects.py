import math, os, sys, time, typing
import numpy as np
from collections import namedtuple

import yaml

"""
Find the closest datum to the midpoint of a filtered star type data track (
created by filter_star_type_data.py).  Print out the data points for log10_L,
R, and Teff

Takes one argument, the name of the star type data track (first argument to
filter_star_type_data.py).
"""
if len(sys.argv) > 1:
    dn = sys.argv[1]
else:
    dn = 'all'

from matplotlib import pyplot as plt
from matplotlib import markers as mk
from matplotlib.colors import colorConverter

xx = [0, 1, 2, 3, 4, 5, 6]
yy = [2, 12, 22, 13, 4, 15, 26]
mm = ['*', 'o', '^', 'v', 's', 'x', 'D']
zz = [4, 6, 10, 4, 7, 5, 12]
cc = ['#ffff00', '#ff00ff', '#ffff00', '#ff0000', '#00ff00', '#0000ff',
      '#00ffff']


def plot(x, y, size, color, marker, title=None):
    plt.plot(x, y)
    if title is not None:
        plt.title(title)
    color = [colorConverter.to_rgb(c) for c in color]
    print(color)
    for i in range(len(x)):
        plt.scatter(x[i], y[i], marker=marker[i], s=size[i] ** 2,
                    c=color[i],
                    alpha=None, edgecolors='black')


data_file = 'star_type_data/star_data-%s.yaml' % dn
Star = namedtuple('Star',
                  't spectral_type stellar_type  Mt Mo Mc Menv R Rc Renv '
                  'log10_L Teff')
StarPeriod = namedtuple('StarPeriod', 'M0 duration star')

print("reading data file %s ..." % data_file)
tstart = time.time()
star_data = yaml.load(open(data_file, 'r').read(), Loader=yaml.CLoader)
tend = time.time()
print("    read data in %0.2f seconds." % (tend - tstart))
print("masses: ", list(star_data.keys()))

SCALE = 20
OFFSET = 3


def duration_fmt(d):
    if d < 10:
        return '%5.3f' % d
    d = round(d)
    return "{:,}".format(d)


def cell_width(duration):
    if duration <= 0:
        return OFFSET * SCALE
    return round((max(-OFFSET, math.log10(duration)) + OFFSET * 2) * SCALE)


def get_mass_tracks():
    result = []
    masses = sorted(star_data.keys())
    for m in masses:
        result.append([])
        for epoch in star_data[m]['epochs']:
            if len(epoch) > 0:
                last_datum = None
                last_spectral_type = None
                final_datum = epoch[-1]
                for idx, datum in enumerate(epoch):
                    if idx == 0 or idx == len(epoch) - 1 or (
                                datum.spectral_type != epoch[
                                    idx - 1].spectral_type):
                        result[-1].append(StarPeriod(m, 0, datum))
                        last_datum = datum
                        last_spectral_type = datum.spectral_type
    return result


def get_duration(mtracks: typing.List[typing.List[StarPeriod]]):
    result = []
    for data in mtracks:
        result.append([])
        for i in range(len(data) - 1):
            dur = data[i + 1].star.t - data[i].star.t
            next = StarPeriod(data[i].M0, dur, data[i].star)
            result[-1].append(next)
    return result


def abbrev(stellar_type):
    if stellar_type == 'LowMassMainSeq' or stellar_type == 'MainSeq':
        return "MS"
    elif stellar_type == 'HertzsprungGap':
        return "HG"
    elif stellar_type == 'GiantBranch':
        return "Giant"
    elif stellar_type.startswith('WhiteDwarf'):
        return "WD"
    return stellar_type


def lifecycle(star_data):
    return '<div class="group">' + ('\n'.join(['<div class="%s" '
                                               'style="width:%dpx"> '
                                               '%s-%s<br>'
                                               'M=%.3f<br>'
                                               'R=%.3f<br>'
                                               'dur=%s<br>'
                                               '</div>' % (d.star.spectral_type,
                                                           cell_width(
                                                               d.duration),
                                                           d.star.spectral_type,
                                                           abbrev(
                                                               d.star.stellar_type),
                                                           d.star.Mt,
                                                           d.star.R,
                                                           duration_fmt(
                                                               d.duration)) for
                                               d in star_data])) + '</div>'


# def plot_lifecycle(strack):
#    for d in strack:
SPECTRAL_COLORS = {
    'O': '#89659D',
    'B': '#37C0D9',
    'A': '#E3FAFC',
    'F': '#FFF7BD',
    'G': '#FFD50E',
    'K': '#C34700',
    'M': '#7F0E00',
    'L': '#520000',
    'T': '#320000',
    'Y': '#100000'
}

STAR_MARKERS = {'LowMassMainSeq':    '*',
                'MainSeq':           '*',
                'HertzsprungGap':    '^',
                'GiantBranch':       'o',
                'CoreHe':            'o',
                'AGB1':              '<',
                'AGB2':              '>',
                'NakedHeMS':         'h',
                'NakedHeHG':         'H',
                'NakedHeGB':         'p',
                'WhiteDwarfHe':      'D',
                'WhiteDwarfCO':      'D',
                'WhiteDwarfON':      'D',
                'NeutronStar':       '+',
                'BlackHole':         'x',
                'MasslessSupernova': ''}
html = ['<html>', '<head>',
        '<style>',
        """
        div.group {
            font-size: 12px;
            margin: 4px;
            border: solid black 1px;
        }
        div.O {
            background-color: #89659D;
            display: inline-block;
        }
        div.B {
            background-color: #37C0D9;
            display: inline-block;
        }
        div.A {

            background-color: #E3FAFC;
            display: inline-block;
        }
        div.F {
            background-color: #FFF7BD;
            display: inline-block;
        }
        div.G {
            background-color: #FFD50E;
            display: inline-block;
        }
        div.K {
            background-color: #C34700;
            display: inline-block;
        }
        div.M {
            color: white;
            background-color: #7F0E00;
            display: inline-block;
        }
        div.L {
            color: white;
            background-color: #520000;
 f           display: inline-block;
        }
        div.T {
            color: white;
            background-color: #320000;
            display: inline-block;
        }
        div.Y {
            color: white;
            background-color: #100000;
            display: inline-block;
        }
        """,
        '</style>',

        '</head>', '<body>']

mtracks = get_mass_tracks()

for track in mtracks:
    track = track[:-1]
    M0 = track[0].M0
    xx = [(d.star.t + 0.0001) for d in track]
    yy = [d.star.R for d in track]
    color = [SPECTRAL_COLORS[d.star.spectral_type] for d in track]
    print([(d.star.t, d.star.spectral_type, SPECTRAL_COLORS[
        d.star.spectral_type],
            d.star.Mt, d.star.R) for d in
           track])
    marker = [STAR_MARKERS[d.star.stellar_type] for d in track]
    size = [max(30 * d.star.Mt, 2) / M0 for d in track]
    title = "$M_0=%03.2f$" % M0
    plot(xx, yy, size, color, marker, title)
    plt.show()

sys.exit(0)
for sd in get_duration(mtracks):
    html.append(lifecycle(sd))

html.extend(['</body>', '</html>'])
open('index.html', 'w').write('\n'.join(html))

sys.exit(0)
types = (('WhiteDwarf', ('WhiteDwarfHe', 'WhiteDwarfCO', 'WhiteDwarfON')),
         ('NeutronStar', ('NeutronStar',)),
         ('BlackHole', ('BlackHole',)),
         ('MasslessSupernova', ('MasslessSupernova',)),
         ('Giant', ('GiantBranch', 'CoreHe', 'AGB1', 'AGB2', 'NakedHeGB')),
         ('MainSeq', ('LowMassMainSeq', 'MainSeq',)),
         )

for label, ob_types in types:
    print()
print(label, '=', get_mass_tracks(ob_types))


def middle_datum(track):
    if len(track) == 0:
        return None
    print('finding best midpoint for M = %f' % track[0].M)

    all_ages = [t.Tev for t in track]
    if len(all_ages) > 1:
        min_age = min(all_ages)
        max_age = max(all_ages)
        mid_age = ((1 - fraction) * min_age + fraction * max_age)
        candidates = [(abs(t.Tev - mid_age), t) for t in track]
        candidates.sort()
        # grab the closest datum to the mid point of the age span
        _, mid = candidates[0]

    else:
        mid = candidates[0]
    return mid

# stars = []
# for m in sorted(star_data.keys()):
# track = star_data[m]
# d = middle_datum(track)
# if d is not None:
# stars.append(d)

# log10_L = [(d.M, d.log10_L, d.stellar_type) for d in stars]
# R = [(d.M, d.R, d.stellar_type) for d in stars]
# Teff = [(d.M,d.Teff, d.stellar_type) for d in stars]
# tmid = [(d.M, d.Tev, d.stellar_type) for d in stars]
# M0_M = [(d.M0, d.M, d.stellar_type) for d in stars]

# print('log10_L =', log10_L)
# print('R =', R)
# print('Teff =', Teff)
# print('tmid =', tmid)
# print('M0_M =', M0_M)
