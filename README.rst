sse-proc
========

Uses [sse-json](https://bitbucket.org/teamcodeviking/sse-json) to generate and
visualize stellar evolution tracks.


Usage
-----

First, you must install 
[sse-json](https://bitbucket.org/teamcodeviking/sse-json).  The sse-json
 executable must be in your PATH.

To generate stellar tracks over all (reasonable) masses:

    cd src/codeviking/astro/sse-proc/generate_tracks
    ./find_epochs_bisect_masses.py
    ./run_all.py
    ./filter_star_type_data.py

The above commands will generate the file: star_data/star_data.yaml, which
can be used by other programs in this suite for a variety of tasks.


The script:

    ./create_epoch_table.py

creates a file called epoch_table.html, which shows epoch types for all
computed stellar tracks.


Notes
-----

The scripts in the visualize/ directory are old, and haven't yet been updated
 to use the new data layout.  
