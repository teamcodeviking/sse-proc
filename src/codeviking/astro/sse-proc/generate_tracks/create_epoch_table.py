#!/usr/bin/env python3
import math, os
from collections import defaultdict
from pprint import PrettyPrinter
from typing import Any, Callable, Dict, List, NamedTuple, Sequence, Tuple
from common import Star, Epoch, StarTrack

import pickle
import yaml
from codeviking.math.interval import RI

INPUT_PATH = '../star_data'
INPUT_FILE = os.path.join(INPUT_PATH, 'star_data.pickle')

# INPUT_FILE = os.path.join(INPUT_PATH, 'star_data.yaml')

stellar_types = {'LowMassMainSeq':    ('orangered', 'black'),
                 'MainSeq':           ('yellow', 'black'),
                 'HertzsprungGap':    ('orange', 'black'),
                 'GiantBranch':       ('red', 'black'),
                 'CoreHe':            ('crimson', 'black'),
                 'AGB1':              ('pink', 'black'),
                 'AGB2':              ('hotpink', 'black'),
                 'NakedHeMS':         ('aqua', 'black'),
                 'NakedHeHG':         ('deepskyblue', 'black'),
                 'NakedHeGB':         ('dodgerblue', 'black'),
                 'WhiteDwarfHe':      ('darkviolet', 'white'),
                 'WhiteDwarfCO':      ('darkmagenta', 'white'),
                 'WhiteDwarfON':      ('indigo', 'white'),
                 'NeutronStar':       ('gray', 'white'),
                 'BlackHole':         ('midnightblue', 'white'),
                 'MasslessSupernova': ('darkblue', 'white')}

"""
def construct_Star(constructor, node):
    mapping = constructor.construct_mapping(node)
    return Star(**mapping)


def construct_Epoch(constructor, node):
    mapping = constructor.construct_mapping(node)
    return Epoch(**mapping)


def construct_StarTrack(constructor, node):
    mapping = constructor.construct_mapping(node)
    return StarTrack(**mapping)


yaml.add_constructor("!Star", construct_Star, Loader=yaml.CLoader)
yaml.add_constructor("!Epoch", construct_Epoch, Loader=yaml.CLoader)
yaml.add_constructor("!StarTrack", construct_StarTrack, 
Loader=yaml.CLoader)"""
# all_data = yaml.load(open(INPUT_FILE, 'r'), Loader=yaml.CLoader)
all_data = pickle.load(open(INPUT_FILE, 'rb'))
print("all_data.keys=" + str(all_data.keys()))
all_tracks = all_data['z_slices'][0]['tracks']


def to_dict(data: Any):
    if type(data) in (str, int, float):
        return data
    if hasattr(data, '_asdict'):
        data = data._asdict()
    if isinstance(data, dict):
        return {k: to_dict(v) for k, v in data.items()}
    if isinstance(data, list) or isinstance(set):
        return [to_dict(i) for i in data]
    return data


print('# tracks = %d' % (len(all_tracks),))
PP = PrettyPrinter(indent=4)
PP.pprint(to_dict(all_tracks[0]))

OFFSET = 4
SCALE = 45
CELL_SIZE = 180


def duration_fmt(d: float) -> str:
    if d >= 1e7:
        d /= 1e6
        return '%3.1fTy' % d
    if d >= 1e4:
        d /= 1e3
        return '%3.1fBy' % d
    if d >= 10:
        return '%3.1fMy' % d
    if d >= 0.01:
        d *= 1000
        return '%3.1fKy' % d
    d *= 1000000
    return '%3.1fy' % d


def radius_fmt(r: float) -> str:
    if r >= 0.1:
        return '%4.2f' % r
    l10r = math.log10(r)
    return '10<sup>%4.1f</sup>' % l10r


def mass_fmt(m: float) -> str:
    if m <= 0.1:
        return '%05.5f' % m
    elif m <= 1:
        return '%05.4f' % m
    elif m <= 10:
        return '%05.3f' % m
    return '%05.2f' % m


def fmt_range(func: Callable[[float], str], a: float, b: float) -> str:
    fa, fb = func(a), func(b)
    if fa == fb:
        return fa
    return '%s - %s' % (fa, fb)


def cell_width(duration: float) -> float:
    if duration <= 0:
        return CELL_SIZE
    ld = math.log10(duration)
    return max(CELL_SIZE, round((ld + OFFSET) * SCALE))


def get_spectral_types(epoch: Epoch) -> list[tuple[str, RI]]:
    ss = epoch.track[0]
    type_seq = [(ss.spectral_type, RI(ss.t, ss.t))]
    for ss in epoch.track:
        st = ss.spectral_type
        lst, lti = type_seq[-1]
        type_seq[-1] = (lst, lti.expand(ss.t))
        if lst != st:
            type_seq.append((st, RI(ss.t, ss.t)))

    # if type_seq[-1][1].length == 0.0:
    #     del type_seq[-1]
    return type_seq


def format_spectral_types(type_seq: Sequence[Tuple[str, RI]]) -> str:
    return 'spectral type: <ul>' + ('\n'.join([f"<li>{st}:  "
                                               f"{duration_fmt(ti.length)}</li>"
                                               for st, ti in
                                               type_seq])) + '</ul>'


def lifecycle(star_track: StarTrack) -> str:
    epochs = []
    for i, e in enumerate(star_track.epochs):
        stypes = get_spectral_types(e)
        s0, s1 = e.track[0], e.track[-1]
        text = ('<br>%s <br>'
                'dur=%s<br>'
                'M= %s M☉<br>'
                'R= %s R☉<br>'
                'T= %d-%dK<br>'
                '%s') % (e.stellar_type,
                         duration_fmt(e.duration),
                         fmt_range(mass_fmt, s0.Mt, s1.Mt),
                         fmt_range(radius_fmt, s0.R, s1.R),
                         round(s0.Teff), round(s1.Teff),
                         format_spectral_types(stypes))
        if i == 0:
            text = ('%08.5f M☉' % s0.Mt) + text
        epochs.append(('<div class="%s" '
                       'style="width:%dpx">%s</div>') % (e.stellar_type,
                                                         cell_width(
                                                                 e.duration),
                                                         text))
    return '<div class="group">' + ('\n'.join(epochs)) + '</div>'


styles = '\n'.join(['''div.%s {
            display: inline-block;
            background-color: %s;
            color: %s;
            padding: 5px;
            border-right: solid black 3px;
            border-top: solid black 3px;
            border-bottom: solid black 3px;
            margin: 0;
            font-size: 11px;
        }''' % (st, c[0], c[1]) for st, c in stellar_types.items()])

html = ['<html>', '<head>',
        '<style>',
        """
        body{
            background: black;
        }
        div.group {
            display: flex;
            font-size: 11px;
            margin: 4px;
        }
        """,
        styles,
        '</style>',

        '</head>', '<body>']

table = [lifecycle(t) for t in all_tracks]

html.extend(table)
html.extend(['</body>', '</html>'])
open('epoch_table.html', 'w').write('\n'.join(html))
