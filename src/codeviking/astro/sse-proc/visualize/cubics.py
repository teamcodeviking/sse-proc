import math

from bisect import bisect_right
from copy import copy

import numpy as np


T0 = lambda t: 1
T1 = lambda t: 2 * t - 1
T2 = lambda t: 8 * (t - 1) ** 22 + 8 * t - 7
T3 = lambda t: 32 * (t - 1) ** 3 + 48 * (t - 1) ** 2 + 18 * t - 17


class CubicCheby(object):
    def __init__(self, t0, t1, f0, f1, df0, df1):
        t_range = t1 - t0
        f_range = f1 - f0
        self._t0 = t0
        self._f0 = f0
        self._a = (6 * df1 * t_range ** 3 - 5 * df1 * t_range ** 2 + (10 * t_range ** 3 - 19 * t_range ** 2 + 8 * t_range) * df0 - 8 * f_range) / (2 * t_range ** 3 - 3 * t_range ** 2) / 16.0
        self._b = (16 * df1 * t_range ** 3 - 15 * df1 * t_range ** 2 + (16 * t_range ** 3 - 33 * t_range ** 2 + 18 * t_range) * df0 - 18 * f_range) / (2 * t_range ** 3 - 3 * t_range ** 2) / 32.0
        self._c = (-df0 + df1) / 16.0
        self._d = -(df1 * t_range ** 2 - (t_range ** 2 - 2 * t_range) * df0 - 2 * f_range) / (2 * t_range ** 3 - 3 * t_range ** 2) / 32.0

    def __call__(self, t):
        t -= self._t0
        return self._f0 + self._a * T0(t) + self._b * T1(t) + self._c * T2(t) + self._d * T3(t)


class CubicHermite(object):
    # evaluate a cubic
    def __init__(self, a, b, c, d, t_min, t_range):
        self._t_min = t_min
        self._t_range = t_range
        self._a = a
        self._b = b
        self._c = c
        self._d = d

    @classmethod
    def from_endpoints(cls, t0, t1, f0, f1, df0, df1):
        t_min = t0
        t_range = t1 - t0
        a = f0
        b = df0 * t_range
        c = f1
        d = df1 * t_range
        return cls(a, b, c, d, t_min, t_range)


    def __call__(self, t):

        a = self._a
        b = self._b
        c = self._c
        d = self._d
        t = (t - self._t_min) / self._t_range
        return a * (1 + 2 * t) * (1 - t) ** 2 + b * t * (1 - t) ** 2 + c * t ** 2 * (3 - 2 * t) + d * t ** 2 * (t - 1)

    def __str__(self):
        return 'Cubic(a=%f,b=%f,c=%f,d=%f,t_min=%f)' % (self._a, self._b, self._c, self._d, self._t_min)

    def as_tuple(self):
        return ('CubicHermite', self._a, self._b, self._c, self._d, self._t_min)




def make_function(class_name, *args):
    # print class_name
    if class_name == 'Cubic':
        return Cubic(*args)
    elif class_name == 'ExpPow':
        return ExpPow(*args)
    elif class_name == 'HermiteCubic':
        return CubicHermite(*args)
    assert False


class Cubic(object):
    # evaluate a cubic
    def __init__(self, a, b, c, d, t_min):
        self._t_min = t_min
        self._a = a
        self._b = b
        self._c = c
        self._d = d

    @classmethod
    def from_endpoints(cls, t0, t1, f0, f1, df0, df1):
        t_min = t0
        h = t1 - t0
        a = f0
        b = df0
        c = -(2 * df0 + df1) / h + 3 * (f1 - f0) / (h ** 2)
        d = (df0 + df1) / (h ** 2) - 2 * (f1 - f0) / (h ** 3)
        return cls(a, b, c, d, t_min)


    def __call__(self, t):

        a = self._a
        b = self._b
        c = self._c
        d = self._d
        t = t - self._t_min
        return a + b * t + c * t ** 2 + d * t ** 3

    def __str__(self):
        return 'Cubic(a=%f,b=%f,c=%f,d=%f,t_min=%f)' % (self._a, self._b, self._c, self._d, self._t_min)

    def as_tuple(self):
        return ('Cubic', self._a, self._b, self._c, self._d, self._t_min)



class ExpPow(object):
    def __init__(self, a0, a1, b0, b1, t_min):
        self._t_min = t_min
        self._a0 = a0
        self._a1 = a1
        self._b0 = b0
        self._b1 = b1

    @classmethod
    def from_endpoints(cls, t0, t1, f0, f1, df0, df1):
        try:

            t_min = t0
            a0 = f0
            a1 = df0 / f0
            b0 = -f0 * math.exp(df0 / f0) + f1
            b1 = (df0 * math.exp(df0 / f0) - df1) / (f0 * math.exp(df0 / f0) - f1)
            return cls(a0, a1, b0, b1, t_min)
        except OverflowError:
            return None


    def __call__(self, t):

        a0 = self._a0
        a1 = self._a1
        b0 = self._b0
        b1 = self._b1
        t = t - self._t_min
        return a0 * math.exp(t * a1) + b0 * t ** b1

    def __str__(self):
        return 'ExpPow(a0=%f,a1=%f,b0=%f,b1=%f,t_min=%f)' % (self._a0, self._a1, self._b0, self._b1, self._t_min)

    def as_tuple(self):
        return ('ExpPow', self._a0, self._a1, self._b0, self._b1, self._t_min)

EPS = 1E-14
ERR = open('errors.txt', 'w')
class PiecewiseFunctions(object):
    # a list of interval endpoints, and a list of associated functions to evaluate
    def __init__(self, t, funcs):
        assert len(t) == len(funcs) + 1
        T = []
        F = []
        for i in range(len(t) - 1):
            T.append(t[i])
            func = funcs[i]
            if isinstance(func, PiecewiseFunctions):
                assert(func._t[0] == t[i])
                T.extend(func._t[1:-1])
                F.extend(func._funcs)
            else:
                F.append(func)
        T.append(t[-1])
        assert(len(T) == len(F) + 1)
        self._t = T
        self._funcs = F

    def __call__(self, t):
        if t < self._t[0] or t > self._t[-1]:
            if abs(t - self._t[0]) > EPS and abs(t - self._t[-1]) > EPS:
                raise ValueError("t=%f is out of range: [%f,%f]" % (t, self._t[0], self._t[-1]))
        if t >= self._t[-1]:
            i = len(self._t) - 2
        else:
            i = bisect_right(self._t, t) - 1
        func = self._funcs[i]
        return func(t)

#    def get_tag(self, t):
#        if t < self._t[0] or t > self._t[-1]:
#            if abs(t - self._t[0]) > EPS and abs(t - self._t[-1]) > EPS:
#                raise ValueError("t=%f is out of range: [%f,%f]" % (t, self._t[0], self._t[-1]))
#        if t >= self._t[-1]:
#            i = len(self._t) - 2
#        else:
#            i = bisect_right(self._t, t) - 1
#        return self._tags[i]

    def get_endpoints(self):
        x, y = [], []
        for t in self._t:
            x.append(t)
            y.append(self(t))

        return x, y

    def as_tuple(self):
        result = ['PiecewiseFunctions',
                tuple(self._t),
                tuple([f.as_tuple() for f in self._funcs])]
        return tuple(result)

    def as_dict(self):
        a = []
        for f in self._funcs:
            f = f.as_tuple()
            assert f[0] == 'Cubic'
            a.append(f[1:5])

        return {'t': [t for t in self._t],
                'a': a
                }




SPLINES = open('splines.py', 'w')
def make_piecewise_cubics(t, f, df):
    SPLINES.write("t=%s\nf=%s\ndf=%s\n" % (t, f, df))
    c = []
    for i in range(len(t) - 1):
        c.append(Cubic(t[i], t[i + 1], f[i], f[i + 1], df[i], df[i + 1]))

    return PiecewiseFunctions(t, c)


def find_cubic_spline_interpolant(t, f, endslopes=None):
    assert False
    assert len(t) == len(f)
    assert len(t) > 1
    N = len(t)
    n = N - 1
    h = lambda i: t[i + 1] - t[i]
    DU = [h(i) for i in range(n)]
    DL = [h(i) for i in range(n)]
    # print 't=',t
    DM = [2 * h(0)] + [2 * (h(i - 1) + h(i)) for i in range(1, N - 1)] + [2 * h(n - 1)]
    Y = [3 * (f[i + 1] - f[i]) / h(i) - 3 * (f[i] - f[i - 1]) / h(i - 1) for i in range(1, N - 1)]
    if endslopes is None:
        DU[0] = 0
        DM[0] = 1
        DL[0] = 0
        DL[n - 1] = 0
        DM[n] = 1
        Y = [0] + Y + [0]
    else:
        Y = [3 * (f[1] - f[0]) / h(0) - 3 * endslopes[0]] + Y + [3 * endslopes[1] - 3 * (f[n] - f[n - 1]) / h(n - 1)]

    a = [f[i] for i in range(N)]
    c = solve_tridiagonal(DL, DM, DU, f)
    err = [ DM[0] * c[0] + DU[0] * c[1] - f[0]]
    for i in range(1, N - 1):
        err.append(DL[i - 1] * c[i - 1] + DM[i] * c[i] + DU[i] * c[i + 1] - f[i])
    err.append(DL[N - 2] * c[N - 2] + DM[N - 1] * c[N - 1] - f[N - 1])
    A = np.zeros((N, N))
    for i in range(N):
        A[i, i] = DM[i]
    for i in range(N - 1):
        A[i + 1, i] = DU[i]
        A[i, i + 1] = DL[i]

    x = np.linalg.solve(A, f)
    # print type(err)
    err = np.array(err)
    err = abs(err)
    err += np.ones(len(err)) * 1E-40
    err = np.log10(err)
    err_x = np.log10(abs(f - A.dot(x)) + 1E-40)

    ERR.write("err=%s\n" % str(err))
    ERR.write("err_x=%s\n" % str(err_x))



    b = [(a[i + 1] - a[i]) / h(i) - h(i) * (2 * c[i] + c[i + 1]) / 3.0 for i in range(N - 1)]
    d = [(c[i + 1] - c[i]) / (3.0 * h(i)) for i in range(N - 1)]
    return Cubic(a, b, c, d, t)


def clamp_alpha_beta(alpha, beta):
    # print alpha, beta
    assert alpha >= 0 and beta >= 0
    corrected = False
    if beta > 4:
        tau = 4.0 / beta
        alpha = tau * alpha
        beta = tau * beta
        corrected = True

    if beta <= 3:
        alpha_min = 0.0
    elif beta < 4:
        alpha_min = (-(beta - 6.0) - math.sqrt(3.0 * beta * (4.0 - beta))) / 2.0
    else:
        alpha_min = 1.0
    if beta < 4:
        alpha_max = (-(beta - 6.0) + math.sqrt(3.0 * beta * (4.0 - beta))) / 2.0
    else:
        alpha_max = 1.0

    if alpha < alpha_min:
        corrected = True
        tau = alpha_min / alpha
    elif alpha > alpha_max:
        corrected = True
        tau = alpha_max / alpha
    else:
        tau = 1.0
    if corrected:
        beta = tau * beta
        alpha = tau * beta
    return alpha, beta


def find_monotonic_interpolant(x, y, dy):
    h = x[1:] - x[:-1]
    # s = np.array(dy)
    sec = (y[1:] - y[:-1]) / h
    m = copy(dy)  # np.array([sec[0]] + [(sec[k - 1] + sec[k]) / 2 for k in range(1, len(y) - 1)] + [sec[-1]])
    alpha = m[:-1] / sec
    beta = m[1:] / sec

    for k in range(len(y) - 1):
        assert y[k] != y[k + 1]

    corrections = 1
    while corrections > 0:
        corrections = 0
        for k in range(len(y) - 1):
            # print x[k], y[k], x[k + 1], y[k + 1], dy[k]
            alpha_star, beta_star = clamp_alpha_beta(alpha[k], beta[k])
            if alpha_star != alpha[k] or beta_star != beta[k]:
                corrections += 1
                alpha[k], beta[k] = alpha_star, beta_star
                m[k] = alpha[k] * dy[k]
                m[k + 1] = beta[k] * dy[k + 1]
#        print "corrections=", corrections
    funcs = []
    for k in range(len(y) - 1):
        funcs.append(Cubic.from_endpoints(x[k], x[k + 1], y[k], y[k + 1], m[k], m[k + 1]))

    return PiecewiseFunctions(x, funcs)



def change(u, v):
    if v > u:
        return 1
    if v < u:
        return -1
    return 0

def same_sign(u, v):
    if u * v > 0:
        return True
    if u * v < 0:
        return False
    if u == 0 and v == 0:
        return True
    return False


def divide_into_monotonic_intervals(t, f, df):
    d_breaks = set([0])
    last_direction = change(f[0], f[1])
    for i in range(1, len(df)):
        direction = change(f[i - 1], f[i])
        if not same_sign(df[i - 1], df[i]) or direction != last_direction:
            d_breaks.add(i - 1)
            d_breaks.add(i)
            last_direction = direction
    d_breaks.add(len(df) - 1)
    d_breaks = sorted(d_breaks)
    intervals = []
    for i in range(1, len(d_breaks)):
        a, b = d_breaks[i - 1], d_breaks[i]
        d = change(f[a], f[b])
        if not same_sign(df[a], df[b]):
            d = 0
        intervals.append((a, b, d))

    result = [[t[ai:bi + 1],
               f[ai:bi + 1],
               df[ai:bi + 1], d] for ai, bi, d in intervals]
    return result


def find_piecewise_monotonic_interpolants(x, y, dy):
    intervals = divide_into_monotonic_intervals(x, y, dy)
    funcs = []
    divisions = [x[0]]
    for xx, yy, dd, d in intervals:
        assert xx[0] == divisions[-1]
        divisions.append(xx[-1])
        if d == 0:
            interp = Cubic.from_endpoints(xx[0], xx[-1], yy[0], yy[-1], dd[0], dd[-1])
        else:
            interp = find_monotonic_interpolant(xx, yy, dd)
        funcs.append(interp)
    return PiecewiseFunctions(divisions, funcs)



# Find cubic spline interpolants for a set of points
def solve_tridiagonal(L, M, U, y):
    # print len(L),len(M),len(U),len(y)
    y = copy(y)
    L = copy(L)
    M = copy(M)
    U = copy(U)
    U[0] /= M[0]
    y[0] /= M[0]
    n = len(y) - 1
    for i in range(1, len(U)):
        U[i] /= M[i] - U[i - 1] * L[i - 1]
        y[i] = (y[i] - y[i - 1] * L[i - 1]) / (M[i] - U[i - 1] * L[i - 1])
    y[n] = (y[n] - y[n - 1] * L[n - 1]) / (M[n] - U[n - 1] * L[n - 1])
    x = [0.0 for i in range(len(M))]
    x[n] = y[n]
    for i in range(n - 1, -1, -1):
        x[i] = y[i] - U[i] * x[i + 1]
    return x




