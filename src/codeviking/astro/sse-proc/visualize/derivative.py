def F3D2_center(x0, x1, x2, f0, f1, f2):
    h0 = x1 - x0
    h1 = x2 - x1
    hh = x2 - x0
    return 2.0 * (((h1 * f0 + h0 * f2) / hh - f1) / (h1 * h0))


def F3D_left(x0, x1, x2, f0, f1, f2):
    h0 = x1 - x0
    h1 = x2 - x1
    hh = h0 + h1
    return -(2.0 * h0 + h1) / (h0 * hh) * f0 + hh / (h0 * h1) * f1 - h0 / (
                hh * h1) * f2


def F3D_right(x0, x1, x2, f0, f1, f2):
    h0 = x1 - x0
    h1 = x2 - x1
    hh = h0 + h1
    return h1 / (hh * h0) * f0 - hh / (h0 * h1) * f1 + (2.0 * h1 + h0) / (
                h1 * hh) * f2


def F3D_center(x0, x1, x2, f0, f1, f2):
    h0 = x1 - x0
    h1 = x2 - x1
    hh = h0 + h1
    return -h1 / (hh * h0) * f0 - (h0 - h1) / (h0 * h1) * f1 + h0 / (
                h1 * hh) * f2


def F5D_0(x, f):
    x1, x2, x3, x4, x5 = x
    f1, f2, f3, f4, f5 = f
    h1, h2, h3, h4 = x1 - x2, x3 - x2, x4 - x3, x5 - x4
    H1, H2 = x4 - x1, x5 - x1


def calc_derivative(t, f):
    N = len(f)
    if N > 2:
        d = [F3D_left(t[0], t[1], t[2], f[0], f[1], f[2])]
        d += [F3D_center(t[i - 1], t[i], t[i + 1], f[i - 1], f[i], f[i + 1])
              for i in range(1, N - 1)]
        d += [F3D_right(t[-3], t[-2], t[-1], f[-3], f[-2], f[-1])]
    else:
        print(N)
        assert N == 2
        d = (t[1] - t[0]) / (f[1] - f[0])
        d = [d, d]
    return d


def calc_derivative2(t, f):
    N = len(f)
    if N > 2:
        d = [F3D2_center(t[0], t[1], t[2], f[0], f[1], f[2])]
        d += [F3D2_center(t[i - 1], t[i], t[i + 1], f[i - 1], f[i], f[i + 1])
              for i in range(1, N - 1)]
        d += [F3D2_center(t[-3], t[-2], t[-1], f[-3], f[-2], f[-1])]
    else:
        raise ValueError(
            "Can't calculate 2nd derivative on a sequence with less than 3 points.")
    return d
