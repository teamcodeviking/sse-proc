import math, os,sys
import numpy as np
from collections import namedtuple

"""
Find the closest datum to the midpoint of a filtered star type data track (created by filter_star_type_data.py).  Print out the data points for log10_L, R, and Teff

Takes one argument, the name of the star type data track (first argument to filter_star_type_data.py).
"""


data_file = 'star_type_data/star_data-%s.py'%sys.argv[1]
Star = namedtuple('Star', 'M Tev type log10_L R Teff stellar_type')
star_data = eval(open(data_file, 'r').read())



def middle_datum(track):
    if len(track)<2:
        return None
    print ('finding best midpoint for M = %f' % track[0].M)

    all_ages = [t.Tev for t in track]
    min_age = min(all_ages)
    max_age = max(all_ages)
    return (track[0].M, math.log10(1e6*(max_age-min_age)))
    
    
stars = []
for m in sorted(star_data.keys()):
    track = star_data[m]
    d = middle_datum(track)
    if d is not None:
        stars.append(d)
    
print(stars)