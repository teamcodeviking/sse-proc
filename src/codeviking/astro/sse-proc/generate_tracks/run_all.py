#!/usr/bin/env python3
import bz2, subprocess, multiprocessing, os, sys, tempfile
import fnmatch

import time
import yaml

"""

"""

evolve_template = """%f %f 20000000.
0.5 0.0 1.0 190.0
1 1 1 1 3.0 999
0.05 0.01 0.02"""
SSE_PATH = ''

INPUT_PATH = '../star_data/'
INPUT_FILE = os.path.join(INPUT_PATH, 'track_samples.yaml')
OUTPUT_PATH = '../raw_track_data/'


def remove_old_output_files():
    output_pattern = "evolve--mass=*--Z=*--.dat.bz2"
    for f in os.listdir(OUTPUT_PATH):
        if fnmatch.fnmatch(f, output_pattern):
            os.remove(os.path.join(OUTPUT_PATH, f))



def run_sse(aa):
    input_text, output_path = aa
    in_handle, in_path = tempfile.mkstemp()
    out_handle, out_path = tempfile.mkstemp()
    os.write(in_handle, input_text.encode())
    os.close(in_handle)
    os.close(out_handle)
    cmd = SSE_PATH + 'sse-json ' + in_path + ' ' + out_path
    (status, output) = subprocess.getstatusoutput(cmd)
    inf = open(out_path, "r")
    outf = bz2.BZ2File(output_path, "w")
    outf.write(inf.read().encode())
    outf.close()
    inf.close()
    os.remove(in_path)
    os.remove(out_path)
    return aa, status, output


def main():

    if not os.path.exists(OUTPUT_PATH):
        os.makedirs(OUTPUT_PATH)

    remove_old_output_files()
    pool = multiprocessing.Pool(processes=None)
    pool_args = []

    stellar_info = yaml.load(open(INPUT_FILE, 'r'))
    mass_digits = stellar_info['mass_digits']
    name_template = "evolve--mass=%%0%d.%df--Z=%%0.4f--.dat.bz2" % (
        mass_digits + 4, mass_digits)
    for mass_group in stellar_info['z_slices']:
        Z = mass_group['Z']
        masses = mass_group['masses']
        print("Generating stellar tracks for Z=%f masses: %s" % (Z, masses))
        for mass in masses:
            input_text = evolve_template % (mass, Z)
            output_path = os.path.join(OUTPUT_PATH,
                                       name_template % (mass, Z))
            pool_args.append((input_text, output_path))

    N = len(pool_args)
    t_start = time.time()
    result = pool.imap_unordered(run_sse, pool_args)
    runs = 0
    fails = 0
    failures = []
    sys.stdout.write("\n")
    for aa, status, output in result:
        runs += 1
        if status != 0:
            fails += 1
            failures.append((status, aa, output))
        sys.stdout.write("\rRun: %d/%d  Failures: %d" % (runs, N, fails))
        sys.stdout.flush()
    pool.close()
    if failures:
        for (status, aa, output) in failures:
            print('\n' + ("=" * 75))
            sys.stdout.write(
                "failure (status=%d) for args: %s\n" % (status, aa))
            sys.stdout.write("output: %s\n" % output)
    pool.join()
    t_end = time.time()
    t_duration = t_end - t_start

    print("\nDuration = %d seconds" % t_duration)


# optimized sse: 116sec

if __name__ == '__main__':
    main()
