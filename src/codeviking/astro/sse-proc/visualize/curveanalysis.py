
from pprint import PrettyPrinter
import numpy as np

PP = PrettyPrinter(indent=4)
from cubics import Cubic

DC_MIN = 0.5;
def f_bump(t, f):
    rf = list()
    for i in range(1, len(f) - 1):
        rf.append(abs((f[i] - f[i - 1]) / (t[i] - t[i - 1]) * (f[i] - f[i + 1]) / (t[i + 1] - t[i])))
    return t[1:-1], rf

def trapezoid_area(t0, t1, f0, f1):
    sh = min(f0, f1)
    h = max(f0, f1)
    th = (h - sh)
    w = t1 - t0
    return w * sh + 0.5 * w * th

def trapezoid_area_squared(t0, t1, f0, f1):
    sh = min(f0, f1)
    h = max(f0, f1)
    th = (h - sh)
    w = t1 - t0
    return (w * sh + 0.5 * w * th) ** 2

def I_3_squared(c):
    return lambda t: 1 / 7.0 * c[0] ** 2 * t ** 7 + 1 / 3.0 * c[0] * c[1] * t ** 6 + 2 / 5.0 * c[0] * c[2] * t ** 5 + 1 / 5.0 * c[1] ** 2 * t ** 5 + 1 / 2.0 * c[0] * c[3] * t ** 4 + 1 / 2.0 * c[1] * c[2] * t ** 4 + 2 / 3.0 * c[1] * c[3] * t ** 3 + 1 / 3.0 * c[2] ** 2 * t ** 3 + c[2] * c[3] * t ** 2 + c[3] ** 2 * t

def I_2_squared(c):
    return lambda t: 1 / 5.0 * c[0] ** 2 * t ** 5 + 1 / 2.0 * c[0] * c[1] * t ** 4 + 2 / 3.0 * c[0] * c[2] * t ** 3 + 1 / 3.0 * c[1] ** 2 * t ** 3 + c[1] * c[2] * t ** 2 + c[2] ** 2 * t

def I_2(c):
    return lambda t: c[0] * t ** 3 / 3.0 + 0.5 * c[1] * t ** 2 + c[2] * t

def I_3(c):
    return lambda t: c[0] * t ** 4 / 4.0 + c[1] * t ** 3 / 3.0 + 0.5 * c[2] * t ** 2 + c[3] * t

# calculate the cubic interpolating polynomial through this point and the two neighbors.  Use the derivative at this point, too.
# calculate the error between the piecewise linear function and the interpolant.
def cubic_interpolant(t0, t1, t2, f0, f1, f2):
    A = np.array([[t0 ** 3, t0 ** 2, t0, 1],
               [t1 ** 3, t1 ** 2, t1, 1],
               [t2 ** 3, t2 ** 2, t2, 1],
               [3 * t1 ** 2, 2 * t2, 1, 0]])
    d = (f2 - 2 * f1 + f0) / (t2 - t0)
    b = np.array([f0, f1, f2, d])
    ip = np.linalg.solve(A, b)
    I = I_3(ip)
    p_area = (I(t1) - I(t0)) - trapezoid_area(t0, t1, f0, f1)
    p_area += (I(t2) - I(t1)) - trapezoid_area(t1, t2, f1, f2)
    return p_area ** 2

def quadratic_interpolant(t0, t1, t2, f0, f1, f2):
    A = np.array([[t0 ** 2, t0, 1],
               [t1 ** 2, t1, 1],
               [t2 ** 2, t2, 1]])
    b = np.array([f0, f1, f2])
    ip = np.linalg.solve(A, b)
    I = I_2(ip)
    p_area = (I(t2) - I(t1)) - trapezoid_area(t0, t1, f0, f1)
    p_area += ((I(t1) - I(t0)) - trapezoid_area(t1, t2, f1, f2))
    return p_area ** 2



def I_error(t, f):
    e = list()
    for i in range(1, len(f) - 1):
        e.append(cubic_interpolant(t[i - 1], t[i], t[i + 1], f[i - 1], f[i], f[i + 1]))

    a = sum(e) / len(f) * 2.0
    if a == 0.0:
        a = 1.0
    e = [y / a for y in e]

    return t[1:-1], e


def tri_area(t, f):
    result = []
    for i in range(1, len(f) - 1):
        a = 0.0
        a += t[i] * f[i - 1] - t[i - 1] * f[i]
        a += t[i + 1] * f[i] - t[i] * f[i + 1]
        a += t[i - 1] * f[i + 1] - t[i + 1] * f[i - 1]
        a /= 2.0
        result.append((a * a))
    a = sum(result) / len(result) * 2.0
    result = [y / a for y in result]
    return t[1:-1], result

def area2_D2(t, ddf):
    t, a = tri_area(t, ddf)
    a = [y * y for y in a]
    return t, a



# AF = 0
# AREA = None


def find_discontinuity_indices(t, ddf, ret_smoothed=False):
    assert len(t) == len(ddf)
    assert len(t) > 1
    # global AREA
    SMOOTH = 5
    smoothed = np.zeros(len(ddf))
    ddf = np.array(ddf)
    for i in range(len(ddf)):
        content = []
        for j in range(-SMOOTH, SMOOTH + 1):
            if i + j >= 0 and i + j < len(ddf) :
                content.append(abs(ddf[i + j]))
        if len(content) > 3:
            content.sort()
            content = content[:-2]

        smoothed[i] = sum(content) / len(content)

    normalized = ddf / smoothed
    i_split = [0]
    for i in range(1, len(ddf) - 1):
        if abs(normalized[i]) > 5:
            assert i_split[-1] < i
            i_split.append(i)
    i_split.append(len(ddf) - 1)
    if not ret_smoothed:
        return i_split
    return (i_split, t, smoothed, normalized)

def split_at_discontinuities(t, f, ddf):
    assert len(t) == len(f) == len(ddf)
    assert len(t) > 1
    # global AREA, AF
    # AREA = open('area_%s.py' % (chr(AF + ord('a'))), 'w')
    # AF += 1
    # AREA.write('\n\n\n\n\n\n\nt=%s\n' % PP.pformat(list(t)))
    # AREA.write('f=%s\n' % PP.pformat(f))
    # AREA.write('ddf=%s\n' % PP.pformat(ddf))

    i_split = find_discontinuity_indices(t, ddf)
    assert len(i_split) > 1
    # AREA.write("i_split=%s\n" % list(i_split))
    # AREA.close()
    # AREA = None
    result = []
    # print "*"*80
    # print 'discontinuities:', i_split
    for j in range(len(i_split) - 1):
        a, b = i_split[j], i_split[j + 1]
        # print a, b
        assert a < b
        result.append((t[a:b + 1], f[a:b + 1]))

    return result


def interval_error(func, t, f, region):
    # print '\n\n\ninterval errors'
    # print 't=',t
    # print 'f=',f
    # calc cubic spline interpolants through given points, and compare with values (t,f)
    # cs = cubic_interpolant(x0,x1,y0,y1,dy0,dy1)
    # cs = find_cubic_spline_interpolants([x0,x1],[y0,y1],[dy0,dy1])
    errors = [0.0]
    for i in range(1, len(t) - 1):
        _, ts = region(t[i], func(t[i]))
        _, tf = region(t[i], f[i])
        tm1, dummy = region(t[i - 1], f[i - 1])
        tp1, dummy = region(t[i + 1], f[i + 1])
        errors.append(abs(ts - tf) * (tp1 - tm1) * .5)

    errors.append(0.0)
#    print errors
#    spline = [ (x, func(x)) for x in t]
#    original = [ (t[i], f[i]) for i in range(len(t))]
#    print 'spline=', spline
#    print 'original=', original
    mxe = max(errors)
    avg = sum(errors) / len(t)
    return avg, mxe

MAX_ERROR = 1E-6
MAX_AVG_ERROR = 1E-7



def knot_weight(a, b, i):
    k = (a + b) / 2.0
    d = 2.0 * (i - k) / (b - a) - 1.0
    return 1.0 + 0.5 * d ** 2

from region import Region

def find_knot_indices(t, f, df, region=None, ai=0, bi= -1):
    # print '\n' * 10
    # print "find_knot_indices"
    # print '-' * 70
    # print 't=', t
    # print 'f=', f
    # print 'df=', df
    if region is None:
        region = Region(None, None)  # Region(t, f)

    assert len(t) == len(f)
    # print '(ai,bi)=', (ai, bi)
    # print 't[ai]=%f, t[bi]=%f, f[ai]=%f, f[bi]=%f' % (t[ai], t[bi], f[ai], f[bi])
    # find an optimal subset of the given points to use as knots for cubic spline interpolation
    if bi == -1:
        bi = len(t) - 1
    assert ai < bi


    cfunc = Cubic.from_endpoints(t[ai], t[bi], f[ai], f[bi], df[ai], df[bi])
#    epfunc = ExpPow.from_endpoints(t[ai], t[bi], f[ai], f[bi], df[ai], df[bi])
    c_avg_err, c_max_err = interval_error(cfunc, t[ai:bi + 1], f[ai:bi + 1], region)
    max_err = c_max_err
    avg_err = c_avg_err

#        ep_avg_err, ep_max_err = interval_error(epfunc, t[ai:bi + 1], f[ai:bi + 1], region)
#        if ep_max_err < c_max_err and ep_avg_err < 3 * c_avg_err:
#            func = epfunc
#            max_err = ep_max_err
#            avg_err = ep_avg_err


    if ai == bi - 1 or (avg_err < MAX_AVG_ERROR and max_err < MAX_ERROR):
        return [ai, bi]
#        return set([((ai, bi), func.as_tuple())])

    # #print "avg_err, max_err=", avg_err, max_err
    # error is too big, we need to split the interval
    knots = set()

    # avg derivative over the interval
    d = (f[bi] - f[ai]) / (t[bi] - t[ai])
    # find point between ai and bi that has the closest derivative to d
    di = (ai + bi) // 2
    ev = abs(df[di] - d)
    for i in range(ai + 1, bi):
        d_error = abs(df[i] - d) * knot_weight(ai, bi, i)
        if d_error < ev:
            ev = d_error
            di = i
    # we split the interval at di
    # print "splitting the interval [%d,%d] at %d.  interval slope=%f" % (ai, bi, di, d)
    knots.update(find_knot_indices(t, f, df, region, ai, di))
    knots.update(find_knot_indices(t, f, df, region, di, bi))
    return sorted(knots)

from derivative import calc_derivative, calc_derivative2
from cubics import find_piecewise_monotonic_interpolants
# CF = 0

def split_and_cull(t, f):
    # global CF
    # CFILE = open('curve_debug_%s.py' % (chr(CF + ord('a'))), 'w')
    ddf = calc_derivative2(t, f)
    # find all the discontinuities
    subfunctions = split_at_discontinuities(t, f, ddf)
    discontinuities = set()
    subfunc = []
    # find knots for each interval
    # print '\n\nnumber of subfunctions=', len(subfunctions)
    # CFILE.write('subfunctions=%s\n' % PP.pformat([ [list(tt), list(ff)] for tt, ff in subfunctions]))
    DEBUG = []
    region = Region(None, None)  # Region(t, f)

    for (st, sf) in subfunctions:
        # print 'st=', st
        # print 'sf=', sf
        sd = calc_derivative(st, sf)
        region = Region(st, sf)  # Region(t, f)
        # print 'st=',st
        # print 'sf=',sf
        discontinuities.add((st[0], sf[0]))
        discontinuities.add((st[-1], sf[-1]))
        knots = find_knot_indices(st, sf, sd, region)
        endpoints = []
        values = []
        derivatives = []
        # print 'number of knots =', len(sorted(knots))
        # print 'knots =', list(sorted(knots))
        for i in knots:
            if st[i] not in endpoints:
                endpoints.append(st[i])
                values.append(sf[i])
                derivatives.append(sd[i])
        # print 'number of knots =', len(endpoints)
        pwfunc = find_piecewise_monotonic_interpolants(np.array(endpoints), np.array(values), np.array(derivatives))
        subfunc.append((st[0], st[-1], (endpoints, values, derivatives), pwfunc))
        DEBUG.append((st[0], st[-1], (endpoints, values, derivatives), pwfunc.as_tuple))
#        subfunc.append((st[0], st[-1], (endpoints, values, derivatives), pwfunc))
#        DEBUG.append((st[0], st[-1], (endpoints, values, derivatives), pwfunc.as_tuple))
    # CFILE.write('subfunctions=%s\n' % PP.pformat(DEBUG))
    # CF += 1
    # CFILE.close()
    return sorted(discontinuities), subfunc, region




# x = np.arange(0, 2 * np.pi, 0.2)
# y = np.sin(x)
# df = calc_derivative(x, y)
# intervals = divide_into_monotonic_intervals(x, y, df)
# print "intervals=", len(intervals)
# import matplotlib.pyplot as plt
# for xx, yy, dd, d in intervals:
#    if d < 0:
#        c = 'g'
#    elif d > 0:
#        c = 'c'
#    else:
#        c = 'm'
#    plt.plot(xx, yy, color=c)
#    plt.plot([xx[0], xx[-1]], [yy[0], yy[-1]], color='k', marker='x', linestyle=' ')
#
# plt.show()
#
# import  matplotlib.pyplot as plt
# from cubics import find_piecewise_monotonic_interpolants
# COLORS = {-1:'c', 0:'m', 1:'g'}
# x = np.arange(0, 2 * np.pi, 0.1)
# F = np.sin
# y = F(x)
# df = calc_derivative(x, y)
# p = find_piecewise_monotonic_interpolants(x, y, df)
# ex, ey = p.get_endpoints()
# et = [COLORS[p.get_tag(xv)] for xv in ex]
# plt.plot(ex, ey, marker='o', linestyle=' ', markersize=12)
#
# xx = np.arange(x[0], x[-1] + 0.01, 0.01)
# yy = F(xx)
#
# plt.plot(xx, yy, linewidth=3)
#
# iy = [p(xv) for xv in xx]
# cc = [p.get_tag(xv) for xv in xx]
# plt.plot(xx, iy, 'k', marker='x')
#
# plt.show()
#
#
#
# ERR = open('errors.txt', 'w')
