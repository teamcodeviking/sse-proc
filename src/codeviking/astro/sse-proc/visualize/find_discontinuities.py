import sys
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt
from numpy.core.numeric import convolve


def appfunc(f, a):
    return np.array([f(x) for x in a])

# function discontinuity
# 1st derivative discontinuity
# 2nd derivative discontinuity
def s1(t):
    if t < 1:
        return t ** 2
    return 2

# function discontinuity
# 1st derivative continuous
# 2nd derivative continuous
def s2(t):
    if t < 1:
        return t ** 2
    return t ** 2 + 1

# function continuous
# 1st derivative discontinuity
# 2nd derivative discontinuity
def s3(t):
    if t < 1:
        return t ** 2
    return (t - 2) ** 2

# function continuous
# 1st derivative discontinuity
# 2nd derivative discontinuity
def s4(t):
    if t < 1:
        return t ** 2
    return t ** 3

# function continuous
# 1st derivative discontinuity
# 2nd derivative continuous
def s5(t):
    if t < 1:
        return t ** 2
    a = -sqrt(3.0) / 9.0
    b = sqrt(3.0) + 1.0
    return a * (t - b) ** 3.0


# function continuous
# 1st derivative continuous
# 2nd derivative discontinuity
# def s6(t):
#    if t < 1:
#        return t ** 2
#    return 2 * t - 1
def s6(t):
   if t < 1:
       return 1.0 / (0.1 + t)
   return (1.0 / 1.1) ** t

# s5(t) = a*sin(t+b)
# s5(1) = 1
# s5'(1) = 2
# a*sin(1+b) = 1
# a*(sin(1)*cos(b) + cos(1)*sin(b)) = 1
# a*(cos(1)*cos(b) + sin(1)*sin(b)) = 2
# a*
# a = 1/sin(b+1)
# a*cos(b+1)/sin(b+1)= 2
# a/2 = tan(b+1)
# a/2 = (tan(b)+tan(1))/(1-tan(b)*tan(1))
# a/2*(1-tan(b)*tan(1)) = tan(b) + tan(1)
# a/2 - tan(1) = (1+tan(1)*a/2)*tan(b)
# tan (b) =


from curveanalysis import *

SCALE = 1.0
T = np.arange(0.5, 1.1, 0.01)
CST = np.arange(0.5, 1.1, 0.002)

S1 = appfunc(s1, T) * SCALE
S2 = appfunc(s2, T) * SCALE
S3 = appfunc(s3, T) * SCALE
S4 = appfunc(s4, T) * SCALE
S5 = appfunc(s5, T) * SCALE
S6 = appfunc(s6, T) * SCALE
plt.figure(1)
funcs = [S1, S2, S3, S4, S5, S6]
plotcol = 1
N = len(funcs)
NP = 3
peak = np.array([-0.5, 1, -0.5])
import pywt
from copy import deepcopy
from cubics import same_sign
import scipy.signal as signal
for F in funcs:
    print '\n' * 8
    discontinuities, subfunctions = split_and_cull(T, F)
    df = calc_derivative(T, F)
    ddf = np.array(calc_derivative2(T, F))
    disc_t = [d[0] for d in discontinuities]
    disc_f = [d[1] for d in discontinuities]
    plt.subplot(NP, N, plotcol)
    plt.plot(T, F, 'k')
    plt.plot(disc_t, disc_f, color='r', marker='x', linestyle=' ', markersize=16, markeredgewidth=3)
    for (a, b, knots, spline) in subfunctions:
        print 'subfunction on [%f,%f]' % (a, b)
        print 'knots=', knots
        plt.plot(*knots[:2], color='g', marker='x', linestyle=' ', markersize=8, markeredgewidth=2)
        ab = np.arange(a, b, 0.01)
        v = [spline(x) for x in ab]
        plt.plot(ab, v, color='orange')

    plt.subplot(NP, N, N + plotcol)
    plt.plot(T, df)
    plt.subplot(NP, N, 2 * N + plotcol)
    plt.plot(T, ddf, marker='o',)


    tzero = 0.1 * max(abs(ddf))


#    for i in range(1, len(ddf)):
#        mn = min(abs(ddf[i - 1]), abs(ddf[i]))
#        mx = max(abs(ddf[i - 1]), abs(ddf[i]))
#        if mn < 0.1 * mx:
#            sc_ddf = 1
#        else:
#            sc_ddf = 0
#
#        if abs(zddf[i]) != 0:
#            csign.append(np.sign(zddf[i]))
#        else:
#            csign.append(0)
#
#
#    csign = []
#    for i in range(0, len(zddf)):
#        if abs(zddf[i]) != 0:
#            csign.append(np.sign(zddf[i]))
#        else:
#            csign.append(0)

    plotcol += 1
plt.show()
