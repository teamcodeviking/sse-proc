#!/usr/bin/env python3
import bisect, bz2, subprocess, multiprocessing, os, sys, tempfile
import concurrent.futures
import typing
from copy import copy

import time
import json
import yaml

"""
This program runs stellar tracks for different trial masses.  If adjacent
tracks are too far apart, another trial track is created between them.  This
is continued until tracks are sufficiently close, or until adjacent masses
are 1/MASS_SCALE apart.  Stellar tracks must have the same epoch sequences 
in order to be considered close.

Once all the tracks are created, they are divided into mass ranges that
contain identical epoch sequences.  Unneeded masses in each mass range are
then culled.  The final output is a sequence of masses that should be used in
the run_all.py program.

"""
evolve_template = """%f %f 20000000.
0.5 0.0 1.0 190.0
1 1 1 1 3.0 999
0.05 0.01 0.02"""
SSE_PATH = ''

io_lock = multiprocessing.RLock()


def _print(s: str):
    global io_lock
    with io_lock:
        sys.stdout.write(s)
        sys.stdout.flush()


OUTPUT_PATH = '../star_data/'
if not os.path.exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)
OUTPUT_FILE = os.path.join(OUTPUT_PATH, 'track_samples.yaml')
stellar_types = {0:  'LowMassMainSeq',
                 1:  'MainSeq',
                 2:  'HertzsprungGap',
                 3:  'GiantBranch',
                 4:  'CoreHe',
                 5:  'AGB1',
                 6:  'AGB2',
                 7:  'NakedHeMS',
                 8:  'NakedHeHG',
                 9:  'NakedHeGB',
                 10: 'WhiteDwarfHe',
                 11: 'WhiteDwarfCO',
                 12: 'WhiteDwarfON',
                 13: 'NeutronStar',
                 14: 'BlackHole',
                 15: 'MasslessSupernova'}

# Star epoch duration deltas are scaled by these factors (for exampe, a
# 10% difference in one epoch might be as important as a 50% difference in
# another epoch.
type_delta_max = {0:  0.5,
                  1:  0.5,
                  2:  2.0,
                  3:  2.0,
                  4:  12.0,
                  5:  2.0,
                  6:  2.0,
                  7:  12.0,
                  8:  4.0,
                  9:  5.0,
                  10: 100.0,
                  11: 100.0,
                  12: 100.0,
                  13: 100.0,
                  14: 100.0,
                  15: 100.0}

# round masses to 5  digits.
ROUND_TO = 5
MASS_SCALE = 10 ** ROUND_TO

# We start by sampling stellar tracks at these masses.  The masses are stored
#  as integers, which are converted to masses by dividing by MASS_SCALE
masses = list(int(round(m * MASS_SCALE))
              for m in
              [0.08, 0.1, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.8, 0.825, 0.84325,
               0.84330, 0.84335, 0.85,
               0.875, 0.9, 0.925, 0.95, 0.975, 1.0, 1.2, 1.4, 1.6, 1.8,
               2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.1, 6.2, 6.3,
               6.4, 6.5, 7.0, 7.5, 8.0, 8.1, 8.2, 8.3, 8.4, 8.5, 9.0, 10.0,
               11.0, 12.0, 13.0, 13.1, 13.2, 13.3, 13.4, 13.5, 13.6, 13.7,
               13.8, 13.9, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 20.5,
               21.0, 21.5, 22.0, 22.5, 23.0, 23.5, 24.0, 25.0, 30.0, 40.0,
               50.0, 51.0, 52.0, 53.0, 54.0, 55.0, 60.0, 70.0, 80.0, 90.0,
               100.0])
"""masses = list(int(round(m * MASS_SCALE))
              for m in
              [0.08, 0.1, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.8])
"""
masses.sort()

Z = 0.02
epoch_length_tol = 0.5

mass_tol = 0.1 ** ROUND_TO


def rint(x):
    return int(round(x * MASS_SCALE))


class EpochIntervals:
    def __init__(self, starting_values):
        starting_values = sorted(starting_values)
        self._manager = multiprocessing.Manager()
        self._epochs = {x: None for x in starting_values}
        self._executor = concurrent.futures.ProcessPoolExecutor()
        self._intervals = {(starting_values[i - 1], starting_values[i])
                           for i in range(1, len(starting_values))}
        self._epoch_intervals = []

    def process(self):
        while len(self._intervals) > 0:
            _print('processing %d intervals.' % len(self._intervals))
            self.compute_new_values()
        self.cull_short_intervals()
        self.calc_epoch_intervals()

    def calc_epoch_intervals(self):
        masses = list(self._epochs.keys())
        masses.sort()
        start_m = masses[0]

        def get_epoch_seq(e_d):
            return tuple(e for e, d in e_d)

        last_es = get_epoch_seq(self._epochs[start_m])
        last_m = masses[0]
        es = None
        for m in masses[1:]:
            es = get_epoch_seq(self._epochs[m])
            if es != last_es:
                self._epoch_intervals.append(((start_m, last_m), es))
                start_m = m
                last_es = es
            last_m = m
        self._epoch_intervals.append(((start_m, last_m), es))

    def remove_interval(self, interval):
        self._intervals.remove(interval)

    def add_interval(self, interval):
        self._intervals.add(interval)

    def cull_same(self, mrange):
        result = []
        a, b = mrange[0], mrange[-1]
        # if (a, b) == (0.814000, 0.843000):
        #     print(a, b)
        ea, eb = self._epochs[a], self._epochs[b]
        d = calc_delta_epoch_length(ea, eb)
        _print("(%d, %d): %f" % (a, b, d))
        n = len(mrange)
        if n <= 2:
            return [a, b]
        if d <= epoch_length_tol:
            print("    discarding %s" % (mrange[1:-1]))
            return [a, b]
        if n == 3:
            _print("    Subdividing at %d" % mrange[1])
            return mrange
        trials = []
        # we'll try to find the best dividing point
        for i in range(1, n - 1):
            c = mrange[i]
            ec = self._epochs[c]
            dac = calc_delta_epoch_length(ea, ec)
            dcb = calc_delta_epoch_length(ec, eb)
            #            _print("ea=%s, ec=%s, dac=%f" % (str(ea), str(ec),
            #            dac))
            _print("dac=%f, dcb=%f" % (dac, dcb))
            d1 = 1.0
            if dcb == 0.0:
                d1 = 0.0
            else:
                d1 = abs(1 - abs(dac / dcb))
            #            _print("ec=%s, eb=%s, dcb=%f" % (ec, eb, dcb))
            if dac == 0.0:
                d2 = 0.0
            else:
                d2 = abs(1 - abs(dcb / dac))

            d = min(d1, d2)
            trials.append((d, i, dac, dcb))
        print("    trials: %s" % trials)
        trials.sort()
        center = trials[0][1]
        print("    Subdividing at %d" % mrange[center])

        result.extend(self.cull_same(mrange[:center + 1]))
        result.extend(self.cull_same(mrange[center:]))
        return result

    def cull_short_intervals(self):
        masses = list(self._epochs.keys())
        masses.sort()

        print("Masses: %s" % masses)
        intervals = [(masses[i - 1], masses[i]) for i in
                     range(1, len(masses))]
        print("Intervals: %s " % intervals)
        breaks = [(a, b)
                  for (a, b) in intervals if not calc_same_epochs(
                    self._epochs[a], self._epochs[b])]
        same = [(breaks[i - 1][1], breaks[i][0]) for i in
                range(1, len(breaks))]
        same = ([(masses[0], breaks[0][0])] + same +
                [(breaks[-1][1], masses[-1])])
        same = [[m for m in masses if a <= m <= b] for (a, b) in same]

        print("Breaks: %s" % breaks)
        print("Same: %s" % same)
        mvalues = []
        for mrange in same:
            mvalues.extend(self.cull_same(mrange))
        cull = sorted([m for m in masses if m not in mvalues])
        print("Culling masses: %s" % cull)
        for m in cull:
            del self._epochs[m]

    def compute_new_values(self):
        ne = [self._executor.submit(run_sse_get_epochs, m, Z)
              for m, v in self._epochs.items() if
              v is None]
        ne = concurrent.futures.wait(ne)[0]
        ne = [e.result() for e in ne]
        for m, z, v in ne:
            self._epochs[m] = v
        # nds = [self._executor.submit(calc_delta_same,
        #                              (a, self._epochs[a]),
        #                              (self._epochs[b], b))
        #        for (a, b) in self._intervals]
        nds = [self._executor.submit(calc_delta_same,
                                     (a, self._epochs[a]),
                                     (b, self._epochs[b]))
               for (a, b) in self._intervals]
        nds = concurrent.futures.wait(nds)[0]
        nds = [ds.result() for ds in nds]
        for (a, b), d, s in nds:
            self.remove_interval((a, b))
            if ((not s) or d >= epoch_length_tol) and (b - a) >= 1:
                c = (a + b) // 2
                if b - c >= 1 and c - a >= 1:
                    self.add_interval((a, c))
                    self.add_interval((c, b))
                    self._epochs[c] = None


def relative_difference(x, y):
    return abs(x - y) / min(abs(x), abs(y))


def calc_delta_epoch_length(ea, eb):
    return max([relative_difference(da, db) / type_delta_max[t]
                for (t, da), (_, db) in zip(ea, eb)])


def calc_same_epochs(ea, eb):
    epa = [e for e, d in ea]
    epb = [e for e, d in eb]
    return epa == epb


def calc_delta_same(aea, beb) -> bool:
    (a, ea), (b, eb) = aea, beb
    is_same = calc_same_epochs(ea, eb)
    if is_same:
        delta = calc_delta_epoch_length(ea, eb)
    else:
        delta = 1.0
    return (a, b), delta, is_same


def run_sse_get_epochs(scaled_mass, z):
    input_text = evolve_template % (scaled_mass / MASS_SCALE, z)
    in_handle, in_path = tempfile.mkstemp(text=True)
    out_handle, out_path = tempfile.mkstemp(text=True)
    os.write(in_handle, input_text.encode())
    os.close(in_handle)
    os.close(out_handle)

    cmd = SSE_PATH + 'sse-json ' + in_path + ' ' + out_path
    (status, output) = subprocess.getstatusoutput(cmd)
    inf = open(out_path, "r")
    print(out_path)
    try:
        data = json.load(inf)['track']
    except:
        inf = open(out_path, "r")
        print(inf.readline())
        print(inf.readline())
        print(inf.readline())
        print(inf.readline())
        sys.exit(-1)
    inf.close()
    os.remove(in_path)
    os.remove(out_path)
    epoch_starts = [tuple(data[0][0:2])]
    for d in data[1:]:
        if epoch_starts[-1][1] != d[1]:
            epoch_starts.append(tuple(d[0:2]))
    epoch_starts.append(data[-1])

    epochs = [
        (epoch_starts[i][1], epoch_starts[i + 1][0] - epoch_starts[i][0])
        for i in range(len(epoch_starts) - 1)]

    epochs = tuple(epochs)
    if status != 0:
        raise SystemError(
                "Error when running sse-json on mass=%f Z=%f. status "
                "code =%d" % (scaled_mass / MASS_SCALE, z, status))
    return scaled_mass, z, epochs


def start_main():
    print("Bisecting mass intervals...")
    epoch_intervals = EpochIntervals(masses)
    t_start = time.time()
    epoch_intervals.process()
    t_end = time.time()
    print("\nFinished Bisecting Intervals.")
    t_duration = t_end - t_start
    print("\nDuration = %d seconds" % t_duration)
    mass_values = sorted(
            [m / MASS_SCALE for m in epoch_intervals._epochs.keys()])
    print('Mass values:')
    print(mass_values)

    outf = open(OUTPUT_FILE, 'w')
    ei = [{'mass_interval': [a / MASS_SCALE, b / MASS_SCALE],
           'epochs':        [stellar_types[e] for e in epochs]} for
          (a, b), epochs in
          epoch_intervals._epoch_intervals]

    result = {'mass_digits': ROUND_TO,
              'z_slices':    [
                  {'Z':               Z,
                   'masses':          mass_values,
                   'epoch_intervals': ei}]}
    yaml.dump(result, outf)


# optimized sse: 190sec

if __name__ == '__main__':
    start_main()
