import math, sys
import matplotlib.pyplot as plt
import numpy as np
from curveanalysis import *

stellar_types = {'LowMassMainSeq': '-',
        'MainSeq': '-',
        'HertzsprungGap': '--' ,
        'GiantBranch': '-.' ,
        'CoreHe': '-.' ,
        'AGB1': '-.' ,
        'AGB2': '-.' ,
        'NakedHeMS': '-.',
        'NakedHeHG': '-.',
        'NakedHeGB': '-.',
         'WhiteDwarfHe': ':' ,
         'WhiteDwarfCO': ':' ,
         'WhiteDwarfON': ':' ,
         'NeutronStar': ':' ,
         'BlackHole': ':',
         'MasslessSupernova': ':'}

f1 = sys.argv[1]
f2 = sys.argv[2]
c1 = int(sys.argv[3])
c2 = int(sys.argv[4])
# d1=eval(open(f1,'r').read())
d2 = eval(open(f2, 'r').read())



hat_kernel = [-1.0, 1.0, -1.0]

def hat_ip(t, f):
    result = []
    for i in range(1, len(f) - 1):
        a = 0.0
        T = [t[i] - t[i - 1], (t[i + 1] - t[i - 1]) / 2.0, t[i + 1] - t[i]]
        F = [f[i] - f[i - 1], f[i], f[i] - f[i + 1]]
        for j in range(len(hat_kernel)):
            a += hat_kernel[j] * F[j] / T[j]
        result.append(a)
    return t[1:-1], result





def split_tracks(data, x1, x2):
    pairs = []

    for (k, elems) in data['tracks'].items():
        t = np.array([elem[x1] for elem in elems])
        f = [elem[x2] for elem in elems]
        pairs.append((t, f))
    return pairs





NP = 1
# p1=plot_args(d1,c1,c2,'r')

tracks = split_tracks(d2, c1, c2)
max_t = 0
for (T, F) in tracks:
    max_t = max(max_t, max(T))
from derivative import F3D_center
tracks = [(T / max_t, F) for (T, F) in tracks]
N = 1
ntracks = 0
for T, F in tracks:
    k = 1
    discontinuities, subfunctions = split_and_cull(T, F)
    disc_t = [d[0] for d in discontinuities]
    disc_f = [d[1] for d in discontinuities]
    plt.subplot(3, N, 1);
    plt.plot(T, F, 'k', marker='+')
    plt.plot(disc_t, disc_f, color='r', marker='x', linestyle=' ', markersize=8, markeredgewidth=2)
    for (a, b, knots, spline) in subfunctions:
        plt.subplot(3, N, 1)
        # print 'subfunction on [%f,%f]' % (a, b)
        # print 'knots=', knots
        plt.plot(*knots[:2], color='g', marker='x', linestyle=' ', markersize=6, markeredgewidth=1)
        ab = np.arange(a, b, (b - a) / 100)
        v = [spline(x) for x in ab]
        dv = calc_derivative2(ab, v)
        plt.plot(ab, v, color='orange')
        plt.subplot(3, N, 2)
        plt.plot(ab, dv, color='r', marker='+')
    d2 = calc_derivative2(T, F)
    plt.subplot(3, N, 3)
    plt.plot(T, d2, color='blue')



    ntracks += 1
    print "num tracks:", ntracks
plt.show()
