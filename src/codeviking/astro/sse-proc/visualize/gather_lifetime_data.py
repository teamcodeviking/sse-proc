import bz2, os, pprint
from collections import namedtuple

PP = pprint.PrettyPrinter(indent=4)
INPUT_DIR = 'unculled'
infiles = list(os.listdir(INPUT_DIR))
infiles.sort()

Star = namedtuple('Star', 'mass, Z, tracks')
Track = namedtuple('Track', 'name, Tstart, Tend')

stars = []
for f in infiles:
    print 'processing ', f
    text = bz2.BZ2File(INPUT_DIR + '/' + f, 'r').read()
    star = eval(text)
    tracks = []
    for tname in star['sequence']:
        track = star['tracks'][tname]
        tracks.append(Track(tname, track[0][0], track[-1][0]))
    tracks = tuple(tracks)
    star = Star(mass=star['mass'], Z=star['Z'], tracks=tracks)
    stars.append(star)
stars.sort()

outf = open('star_lifetimes.py', 'w')
outf.write(PP.pformat(stars))
