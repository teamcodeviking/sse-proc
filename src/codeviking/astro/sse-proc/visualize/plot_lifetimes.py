'''
Created on Feb 6, 2013

@author: dan
'''
import sys
from collections import namedtuple
from math import log10

Zshow = float(sys.argv[1])

Star = namedtuple('Star', 'mass, Z, tracks')
Track = namedtuple('Track', 'name Tstart, Tend')


stellar_colors = {
        'LowMassMainSeq': (1, 0, 0),
        'MainSeq': (1, 1, 0),
        'HertzsprungGap': (1.0, 0.5, 0),
        'GiantBranch': (0, 0.6, 0) ,
        'CoreHe': (0.2, 1, 0.2) ,
        'AGB1': (0.5, 1, 0.5) ,
        'AGB2': (0.8, 1, 0.8) ,
        'NakedHeMS': (1, 0, 1),
        'NakedHeHG': (1, 0.33, 1),
        'NakedHeGB': (1, 0.67, 1),
        'WhiteDwarfHe': (0.5, 0.5, 1),
        'WhiteDwarfCO': (0.8, 0.8, 1),
        'WhiteDwarfON': (1, 1, 1),
        'NeutronStar': (0.5, 0, 0.5),
        'BlackHole': (0, 0, 0),
        'MasslessSupernova': (0, 0, 1)}

stars = eval(open('star_lifetimes.py', 'r').read())

import matplotlib.pyplot as plt


type_bottom = {k:list() for k in stellar_colors.keys()}
type_length = {k:list() for k in stellar_colors.keys()}
xc = {k:list() for k in stellar_colors.keys()}

X = range(len(stars))
i = 0
durations = []
OFFSET = 5
for star in stars:
    x = X[i]
    y = 0
    if star.Z != Zshow:
        continue
    for track in star.tracks:
        type_bottom[track.name].append(log10(track.Tstart))
        dy = log10(track.Tend - track.Tstart) + OFFSET
        print dy
        durations.append(dy)
        type_length[track.name].append(log10(track.Tend) - log10(track.Tstart))
        y += dy
        xc[track.name].append(x)
    i += 1
mnd = min(durations)
mxd = max(durations)
print mnd, mxd

for name, color in stellar_colors.items():
    plt.bar(xc[name], type_length[name], bottom=type_bottom[name], color=color)
plt.show()
