from collections import namedtuple
from typing import NamedTuple, Sequence

Star = NamedTuple('Star',
                  [('t', float),
                   ('spectral_type', str),
                   ('stellar_type', str),
                   ('Mt', float),
                   ('M0', float),
                   ('Mc', float),
                   ('Menv', float),
                   ('R', float),
                   ('Rc', float),
                   ('Renv', float),
                   ('log10_L', float),
                   ('Teff', float)])

Epoch = NamedTuple('Epoch',
                   [('stellar_type', str),
                    ('start_time', float),
                    ('duration', float),
                    ('track', Sequence[Star])])

StarTrack = NamedTuple('StarTrack',
                       [('M0', float),
                        ('Z', float),
                        ('epochs', Sequence[Epoch])])
